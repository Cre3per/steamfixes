/**
 * Loads scripts into a webpage
 */
class Injector
{
	/**
	 * Gets called whenever a script has been loaded
	 */
	onScriptLoaded()
	{
		if ((this.pendingScripts === 0) && this.lastScript)
		{
			this.injectLocalScript(this.lastScript.scriptPath, this.lastScript.callback);
			this.lastScript = undefined;
		}
	}

	/**
	 * @name Injector~injectLocalScriptCallback
	 * @function
	 */
	/**
	 * Injects a script contained in the extension
	 * @param {string} scriptPath Local path to the script
	 * @param {Injector~injectLocalScriptCallback} callback Gets called once the script has been loaded
	 */
	injectLocalScript(scriptPath, callback)
	{
		// There's no jQuery loaded yet, use DOM instead

		let script = document.createElement('script');
		
		++this.pendingScripts;
		script.src = browser.extension.getURL(scriptPath);
		script.onload = () =>
		{
			--this.pendingScripts;

			this.onScriptLoaded();
		};

		document.head.appendChild(script);
	}

	/**
	 * @name Injector~injectLocalStyleCallback
	 * @function
	 */
	/**
	 * Injects a script contained in the extension
	 * @param {string} stylePath Local path to the stylesheet
	 * @param {Injector~injectLocalStyleCallback} callback Gets called once the style has been loaded
	 */
	injectLocalStyle(stylePath, callback)
	{
		// There's no jQuery in this context, use DOM instead

		let style = document.createElement('link');
		
		style.href = browser.extension.getURL(stylePath);
		style.rel = 'stylesheet';
		style.type = 'text/css';

		document.head.appendChild(style);
	}

	/**
	 * Calls {@link injectLocalScript} once all other scripts have been loaded
	 * @param {string} scriptPath Local path to the script
	 * @param {Injector~injectLocalScriptCallback} callback Gets called once the script has been loaded
	 */
	 injectLocalScriptLast(scriptPath, callback)
	 {
		if (this.pendingScripts === 0)
		{
			this.injectLocalScript(scriptPath, callback);
		}
		else
		{
			this.lastScript = { scriptPath, callback };
		}
	}

	 /**
	  * Constructor
	  */
	constructor()
	{
		/**
		 * How many scripts have been injected but not loaded
		 * @type {number}
		 */
		this.pendingScripts = 0;

		/**
		 * If a script has been requested to be loaded at last and there were pending scripts, this is it.
		 * @type {object}
		 */
		this.lastScript = undefined;
	}
}