class CFixBigInventory
{
  /**
   * Causes an update of the 'inventories' div
   */
  forceDOMUpdate()
  {
    let inventories = document.getElementById('inventories');
    
    let originalMarginBottom = inventories.style.marginBottom;
    
    inventories.style.marginBottom = "0px";

    setTimeout(() =>
    {
      inventories.style.marginBottom = originalMarginBottom;
    }, 0);
  }

  /**
   * Hooked HideTagFilters
   */
  hkHideTagFilters()
  {
    this.forceDOMUpdate();

    // Restore and call the original HideTagFilters function
    HideTagFilters = this.oHideTagFilters;
    HideTagFilters();
  }

  /**
   * Installs the HideTagFilters hook in economy.js
   */
  installHook()
  {
    this.oHideTagFilters = HideTagFilters;

    HideTagFilters = this.hkHideTagFilters.bind(this);
  }

  /**
   * Waits for economy.js to be loaded and calls {@link installHook}
   */
  waitForEconomy()
  {
    // INVENTORY_PAGE_ITEMS is defined in economy.js, we can use it to check
    // if economy.js has been loaded

    if (typeof(INVENTORY_PAGE_ITEMS) === 'undefined')
    {
      setTimeout(() => { this.waitForEconomy(); }, 10);
    }
    else
    {
      this.installHook();
    }
  }

  /**
   * Default constructor
   */
  constructor()
  {
    /**
     * Original HideTagFilters function
     * @type {function}
     */
    this.oHideTagFilters = undefined;

    this.waitForEconomy();
  }
}

new CFixBigInventory();