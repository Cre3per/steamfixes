# Problem Description

When initially loading the page the item box is too high.

# Steps to reproduce

I am unable to reproduce this bug in chromium version 61.
It can be reproduced in Firefox Developer Edition version 57.

Open a new tradeoffer page (ex. https://steamcommunity.com/tradeoffer/new/?partner=106497991&token=X8HQsOZK)

# Behavior

![bahavior](problem.bmp)

# Expected Behavior

![expected behavior](solved.bmp)

# Cause

Unknown

# Solution Idea

This appears to be a problem with the browser as toggling any style related to the div will reset the control to it's proper height.

# Clientside Solution

- Hook _HideTagFilters_ in [economy.js](https://steamcommunity-a.akamaihd.net/public/javascript/economy.js?v=i8iKSj_RdU-r&l=english). At this point the div will be too large already.
- Inside the hook: Cause a DOM update and unhook

# Serverside Solution

Unknown