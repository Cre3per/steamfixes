class CFixRevertFilter
{
  /**
   * Overwrites RevertItem in economy_trade.js with a working version.
   */
  overwriteRevertItem()
  {
    RevertItem = (item) =>
    {
      if (item.is_stackable)
      {
        SetStackableItemInTrade(item, 0);
      }
      else
      {
        item.homeElement.insertBefore(item.element.remove(), item.homeElement.firstChild);

        let isSameAppId = (g_ActiveInventory.appid == item.appid);
        let isSameContextId = (g_ActiveInventory.contextid == item.contextid);

        if (g_ActiveInventory && isSameAppId && isSameContextId)
        {
          Filter.ApplyFilter($('filter_control').value, item.element);
        }
      }
    };
  }

  /**
   * Waits for economy_trade.js to be loaded and calls {@link overwriteRevertItem}
   */
  waitForEconomyTrade()
  {
    // TRADE_UPDATE_INTEVRAL is defined in economy_trade.js,
    // we can use it to check if economy_trade.js has been loaded.
    // Yes, the variable (Yes, Valve decided this should be a variable, not
    // a constant) name is misspelled.

    if (typeof(TRADE_UPDATE_INTEVRAL) === 'undefined')
    {
      setTimeout(() => { this.waitForEconomyTrade(); }, 10);
    }
    else
    {
      this.overwriteRevertItem();
    }
  }

  /**
   * Default constructor
   */
  constructor()
  {
    /**
     * Original HideTagFilters function
     * @type {function}
     */
    this.oHideTagFilters = undefined;

    this.waitForEconomyTrade();
  }
}

new CFixRevertFilter();