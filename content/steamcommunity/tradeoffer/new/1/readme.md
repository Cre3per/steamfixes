# Problem Description

Items filters don't apply to items that have been in the selected-items-grid
before.

# Steps to reproduce

- Open a new tradeoffer page (ex. https://steamcommunity.com/tradeoffer/new/?partner=106497991&token=X8HQsOZK)

- Select an item by double clicking it.

- Remove the same item by double clicking it.

- Apply a filter the would usually include the item.

# Behavior

The item will not be included by the filter.

# Expected Behavior

The item will be included by the filter.

# Cause

When filtering, Valve expects the first child of the item holder to be the item:
> [economy.js](https://steamcommunity-a.akamaihd.net/public/javascript/economy.js)
>> Filter::ApplyFilter
>>> `var elItem = elItemHolder.firstChild;`

When removing an item from the selected-items-grid, it will be appended to it's home element's child list:
> [economy_trade.js](https://steamcommunity-a.akamaihd.net/public/javascript/economy_trade.js)
>> RevertItem
>>> `item.homeElement.appendChild( item.element.remove() );`

# Solution Idea

Re-order `item.homeElement.children` in a way that the item is the first child.

# Clientside Solution

- Overwrite _RevertItem_ in [economy_trade.js](https://steamcommunity-a.akamaihd.net/public/javascript/economy_trade.js) to apply the [Serverside Solution](#serverSolution)

# Serverside Solution<a name="serverSolution"></a>

Change `appendChild` to `insertBefore` in
> [economy_trade.js](https://steamcommunity-a.akamaihd.net/public/javascript/economy_trade.js)
>> RevertItem
>>> ```javascript
>>> // item.homeElement.appendChild( item.element.remove() );
>>> item.homeElement.insertBefore( item.element.remove(), item.homeElement.firstChild );
>>> ```