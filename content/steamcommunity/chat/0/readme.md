# Problem Description

When sending or receiving a message, the chat will not scroll to the bottom.

# Steps to reproduce

- Open the [web chat](https://steamcommunity.com//chat/)
- Send or receive messages until the message list is filled
- Send or receive a message

# Behavior

The message will be appended to the bottom of the chat but the chat will not scroll to the message.

# Expected Behavior

The chat will scroll to the message if it was previously scrolled all the way to the bottom.

# Cause

`CWebChatDialog.prototype.ScrollToBottom` will override it's `elScrollContainer` with `document.body`.
> [chat.js](https://steamcommunity-a.akamaihd.net/public/javascript/chat.js)
>> CWebChatDialog.prototype.ScrollToBottom
>>> ```javascript
>>> if ( this.m_elContent.css('position') == 'static' )
>>> {
>>>   elScrollContainer = document.body;
>>>   nClientHeight = $J(window ).height();
>>> }
>>> ```
I don't know how Valve originally wanted to do this, I didn't dig any deeper.

Also, the check if the chat is currently scrolled to the bottom is broken.
> [chat.js](https://steamcommunity-a.akamaihd.net/public/javascript/chat.js)
>> CWebChatDialog.prototype.AppendChatMessage for the same reason as above
>>> ```javascript
>>> if ( this.m_elContent.css('position') == 'static' )
>>> {
>>>   elScrollContainer = document.body;
>>>   nClientHeight = $J(window ).height();
>>> }
>>> ```


# Solution Idea

Change the `position` style property of `chat_dialog_content_inner` so it is no longer `static`.

# Clientside Solution

Change the `position` style property of `chat_dialog_content_inner` to `absolute`.

# Serverside Solution

Change the `position` style property of `chat_dialog_content_inner` so it is no longer `static`.